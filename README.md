# `basicpkg`

The `basicpkg` is a simple testing example to understand the basics of developing your first Python package. 

## Install & Usage

You can install `basicpkg` as follows:

```
pip install --index-url https://test.pypi.org/simple/ --no-deps basicpkg
```
Try to have fun with `basicpkg`:

```
from multiply.by_three import multiply_by_three
from divide.by_three import divide_by_three

multiply_by_three(9)
divide_by_three(21)
```

## Useful Resources

* [Packaging Python Projects¶](https://packaging.python.org/en/latest/tutorials/packaging-projects/)
* [Configuring setuptools using setup.cfg files](https://setuptools.pypa.io/en/latest/userguide/declarative_config.html)

